import bpy


class ExportChannelStrips(bpy.types.Operator):
    """Batch exports seperate audio sequences based on the using the audio sequences in active channel as frame ranges.\nMP3 format."""
    bl_idname = "sequencer.export_channel_strips"
    bl_label = "Output Audio Sequences from Channel" 

    def getStartFrame(element):
        return element.frame_final_start

    def execute(self, context):

        print("Say Hello start")

        channelStrips = [] # in channel
        seq = context.scene.sequence_editor 
        channel = seq.active_strip.channel #channel from active strip

        #filters strips based on active strip channel
        for strip in seq.sequences_all:
            if (strip.channel == channel):
                if not strip.mute:
                    if (strip not in channelStrips):
                        if(strip.type == "SOUND"):
                            channelStrips.append(strip)
        
        #print("Number of channel strips: " + str(len(channelStrips)) 

        for strip in channelStrips:
            start = strip.frame_final_start
            end =   strip.frame_final_end
            bpy.context.scene.frame_start = start
            bpy.context.scene.frame_end =   end
            fileName = strip.name
            if not (strip.name[-4:].lower() == ".mp3"):
                fileName = fileName +  ".mp3"
            exportPath = context.scene.render.filepath + fileName

            print("exporting - " + exportPath)
            bpy.ops.sound.mixdown(
                filepath=exportPath,
                check_existing=False,
                relative_path=False, 
                container='MP3',
                codec='MP3',
                bitrate=192
                )

        return {'FINISHED'}
    


    
