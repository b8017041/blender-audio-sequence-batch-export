import bpy
#import ExportChannelStrips

class ExportPanel(bpy.types.Panel):
    """A Sequence Editor Panel for batch exporting a audio sequences."""
    bl_label =  "Output Audio Strips"
    bl_idname = "SCENE_PT_export_audio_strips"
    bl_space_type = "SEQUENCE_EDITOR"
    bl_region_type = "UI"
    bl_category = 'Strip'

    @classmethod
    def poll(cls, context):
        return context.space_data.view_type == 'SEQUENCER' and len(context.selected_sequences) == 1

    def draw(self, context):

        #Variables
        scene = context.scene
        layout = self.layout
        #channel = context.scene.sequence_editor.active_strip.channel

        #structure
        layout.prop(context.scene.render, "filepath", text = "")
        layout.label(text = "Active Channel: " + str(context.scene.sequence_editor.active_strip.channel))

        layout.operator("sequencer.export_channel_strips", text = "Render Audio Strips")

        return None
    
    


