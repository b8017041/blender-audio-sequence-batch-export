import bpy
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

class AudioSequenceBatch(bpy.types.Operator):
    bl_idname = "object.audioSequenceBatch"
    bl_label = "Audio Sequence Batch"

    def execute(self, context):
        return {'FINISHED'}

bl_info = {
    "name" : "Audio Sequence Batch",
    "author" : "Luc",
    "description" : "",
    "blender" : (2, 80, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Import-Export"
}

import bpy

from . operators import ExportChannelStrips
from . gui import ExportPanel

def register():
    bpy.utils.register_class(ExportChannelStrips)
    bpy.utils.register_class(ExportPanel)

def unregister():
    bpy.utils.unregister_class(ExportChannelStrips)
    bpy.utils.unregister_class(ExportPanel)

if __name__ == "__main__":
    register()